package com.nsoff.myapplication.Adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nsoff.myapplication.R

/**
 * Created by Sin on 2019/1/20
 */
class ListAdapter(val datas: List<String>, val context: Context) : RecyclerView.Adapter<ListAdapter.InnerHolder>() {

    /**
     * 相当于getView()
     */
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ListAdapter.InnerHolder {
        //加载View
        var itemView: View = LayoutInflater.from(context).inflate(R.layout.activity_my_recycler_item, p0, false)
        return InnerHolder(itemView)

    }

    /**
     * 得到总条数
     */
    override fun getItemCount(): Int = datas.size

    class InnerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemText: TextView = itemView.findViewById(R.id.item_tv)

    }

    /**
     * 绑定数据，View和数据绑定
     */
    override fun onBindViewHolder(p0: ListAdapter.InnerHolder, p1: Int) {
        //设置数据
        p0?.itemText?.text = datas[p1]

    }

}