package com.nsoff.myapplication.base

import android.content.Context
import org.jetbrains.anko.toast

open class Animal   {
    constructor()
    {}
    constructor(context: Context, name:String)
    {
         context.toast("这是只$name")
    }
    constructor(context:Context,name:String,sex:Int){
        var sexName:String=if(sex==0) "公" else "母"
        context.toast("这只${name}是${sexName}的")
    }

    var sexname:String=""

    companion object WildAnimal{
        fun JudgeSex(sexName:String):Int{
            var sex:Int=when(sexName)
            {
                "公","雄"->0
                "母","雌"->1
                else ->-1
            }
            return sex
        }
        var male=0
    }
}

class Dog:Animal()
{

}
