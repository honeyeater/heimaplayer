package com.nsoff.myapplication.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.toast


abstract class BaseActivity:AppCompatActivity(),AnkoLogger{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        initListener()
    }

    /**
     * 获取布局id
     */
    abstract fun getLayoutId(): Int

    protected  open fun initListener(){

    }


    /**
     * 考虑线程的Toast方法
     */
    fun myToast(msg: String) {
        runOnUiThread { toast(msg) }
    }
}