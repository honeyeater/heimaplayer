package com.nsoff.myapplication.model



data class MemberItemBean(var name:String,
                          var phone:String,
                          var address:String,
                          var createTime:String,
                          var updateTime:String,
                          var id:Int)
