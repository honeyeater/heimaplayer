package com.nsoff.myapplication.net

import com.nsoff.myapplication.model.MemberItemBean
import com.nsoff.myapplication.util.URLProviderUtils


class HomeRequest(type:Int,offset:Int,handler:ResponseHandler<List<MemberItemBean>>):
        MRequest<List<MemberItemBean>>(type, URLProviderUtils.GetHomeUrl(offset,20),handler)