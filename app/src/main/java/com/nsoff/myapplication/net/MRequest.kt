package com.nsoff.myapplication.net

import com.google.gson.Gson
import java.lang.reflect.ParameterizedType

open class MRequest<T>(val type:Int,val url:String,val handler:ResponseHandler<T>)
{


    /*
    解释网络请求结果
    * */
    fun parseResult(result:String?):T{
        val gson= Gson()
        val type=(this.javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0]
        return gson.fromJson<T>(result,type)
    }

    /*
    发送网络请求
    * */
    fun execute(){
        NetManager.manager.sendRequest(this)
    }
}