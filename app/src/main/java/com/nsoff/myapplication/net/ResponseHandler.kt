package com.nsoff.myapplication.net

interface ResponseHandler<T>{
    fun onError(type:Int,msg:String?)
    fun onSuccess(type:Int,result:T)
}